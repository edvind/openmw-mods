# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1
from pybuild.info import P

from common.util import CleanPlugin


class Package(CleanPlugin, Pybuild1):
    NAME = "True Lights and Darkness"
    DESC = "Darkens all interior cells and increases radius of all light sources"
    HOMEPAGE = """
        http://mw.modhistory.com/download--6771
        https://www.nexusmods.com/morrowind/mods/39605
    """
    # Incorporates parts of Mudcrab Island (which can be freely used,
    # except for some unlicensed assets, but TLAD only uses scripts from it)
    # Also incorporates parts of Lights300, which is unlicensed
    # TLAD itself has an attribution license, but this only applies to Booze's own work
    LICENSE = "attribution all-rights-reserved"
    RDEPEND = "base/morrowind[bloodmoon,tribunal]"
    DEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = f"http://mw.modhistory.com/file.php?id=6771 -> {P}.zip"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/39605"
    IUSE = "+daylight"
    SETTINGS = {
        "Shaders": {"clamp lighting": "false"},
    }
    FALLBACK = {
        "LightAttenuation": {
            "UseConstant": 1,
            "ConstantValue": 0.382,
            "UseLinear": 1,
            "LinearMethod": 1,
            "LinearValue": 1,
            "LinearRadiusMult": 1.0,
            "UseQuadratic": 1,
            "QuadraticMethod": 2,
            "QuadraticValue": 2.619,
            "QuadraticRadiusMult": 1.0,
            "OutQuadInLin": 0,
        }
    }

    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[
                # Requires mudcrab island
                # File("Mudcrab Island TLAD.esp"),
                File(
                    "True_Lights_And_Darkness_1.0-NoDaylight.esp",
                    REQUIRED_USE="!daylight",
                ),
                File("True_Lights_And_Darkness_1.1.esp", REQUIRED_USE="daylight"),
            ],
        ),
    ]

    def src_prepare(self):
        # self.clean_plugin("Mudcrab Island TLAD.esp")
        if "daylight" in self.USE:
            self.clean_plugin("True_Lights_And_Darkness_1.1.esp")
        else:
            self.clean_plugin("True_Lights_And_Darkness_1.0-NoDaylight.esp")

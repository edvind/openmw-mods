# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import InstallDir, Pybuild1
from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Vivec and Velothi - Arkitektora Vol.2"
    DESC = "Textures of Vivec City and Velothi Architecture"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46266"
    KEYWORDS = "openmw"
    LICENSE = "attribution"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/46266"
    TEXTURE_SIZES = "1024 2048 4096"
    SRC_URI = """
        texture_size_1024? (
            Vivec_and_Velothi_Arkitektora__-_MQ-46266-2-0-1546272664.rar
        )
        texture_size_2048? (
            Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606.rar
        )
        texture_size_4096? (
            Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606.rar
        )
        atlas? (
            texture_size_1024? (
                Vivec_and_Velothi_Arkitektora__-_ATLAS_HQ-46266-1-0-1546272773.rar
            )
            texture_size_2048? (
                Vivec_and_Velothi_Arkitektora__-_ATLAS_MQ-46266-1-0-1546272827.rar
            )
            texture_size_4096? (
                Vivec_and_Velothi_Arkitektora__-_ATLAS_UHQ-46266-1-0-1546273178.rar
            )
        )
    """
    IUSE = "atlas"
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_MQ-46266-2-0-1546272664",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_HQ-46266-2-0-1546272606",
            REQUIRED_USE="|| ( texture_size_2048 texture_size_4096 )",
        ),
        InstallDir(
            "ATL MQ",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_MQ-46266-1-0-1546272827",
            REQUIRED_USE="texture_size_1024 atlas",
        ),
        InstallDir(
            "ATL HQ",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_HQ-46266-1-0-1546272773",
            REQUIRED_USE="texture_size_2048 atlas",
        ),
        # Author notes that this UHQ atlas texture is very demanding on most graphics
        # cards however it is unknown if that is a problem for openmw as much as it is
        # for morrowind.
        InstallDir(
            "Data Files",
            S="Vivec_and_Velothi_Arkitektora__-_ATLAS_UHQ-46266-1-0-1546273178",
            REQUIRED_USE="texture_size_4096 atlas",
        ),
    ]

# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
from common.system import System
from pybuild.info import PN


class Package(System):
    NAME = "ImageMagick"
    DESC = "A command line tool for manipulating images"
    HOMEPAGE = "https://imagemagick.org"
    RELEASE_PAGE = "https://imagemagick.org/script/download.php"
    KEYWORDS = "openmw tes3mp"

    def pkg_pretend(self):
        super().pkg_pretend()
        exec_path = shutil.which(self.PN)

        if not exec_path:
            self.warn(
                """Imagemagick 6 might be installed, but it won't be detected or """
                """used as it doesn't provide the "magick" command.\n"""
                "Imagemagick 7 or newer is required.\n"
            )

    def get_version(self):
        out = self.execute("magick --version", pipe_output=True)
        for line in out.splitlines():
            if line.startswith("Version: "):
                return line.split()[2].replace("-", ".")

        raise Exception(f"Could not determine version of {PN}!")
